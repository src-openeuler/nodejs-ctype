%{?nodejs_find_provides_and_requires}
%global commit 3aae5f7aa45906cfcb283817cfb6fcb15360391d
%global shortcommit %(c=%{commit}; echo ${c:0:7})
Name:                nodejs-ctype
Version:             0.5.3
Release:             1
Summary:             Read and write binary structures and data types with Node.js
License:             MIT
URL:                 https://github.com/rmustacc/node-ctype
Source0:             https://github.com/rmustacc/node-ctype/archive/%{commit}/nodejs-ctype-%{version}-3aae5f7.tar.gz
Patch1:              nodejs-ctype-README.patch
BuildArch:           noarch
BuildRequires:       nodejs-packaging
%description
Node-CType is a way to read and write binary data in a structured and easy to
use format. Its name comes from the C header file.
There are two APIs that you can use, depending on what abstraction you'd like.
The low level API lets you read and write individual integers and floats from
buffers. The higher level API lets you read and write structures of these.

%prep
%autosetup -n node-ctype-%{commit}

%build

%install
mkdir -p %{buildroot}%{nodejs_sitelib}/ctype
cp -pr package.json ctf.js ctio.js ctype.js %{buildroot}%{nodejs_sitelib}/ctype
mkdir -p %{buildroot}%{_mandir}/man3
cp -pr man/man3ctype/ctio.3ctype %{buildroot}%{_mandir}/man3/ctio.3
%nodejs_symlink_deps

%check
pushd tst
for dir in ctf ctio/* ctype; do
    pushd $dir
    for f in *.js; do
        %{__nodejs} $f
    done
    popd
done
popd

%files
%{nodejs_sitelib}/ctype
%{_mandir}/man3/ctio.3.*
%doc CHANGELOG README README.old
%license LICENSE

%changelog
* Mon Aug 17 2020 Anan Fu <fuanan3@huawei.com> - 0.5.3-1
- package init
